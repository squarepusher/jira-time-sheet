# jira-time-sheet

Simple but convenient JIRA time sheet bookmarklet. Allows you to see hours reported by you/your team.